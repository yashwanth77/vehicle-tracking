package com.vehicletracking.provider;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vehicletracking.model.Apartment;
import com.vehicletracking.repository.ApartmentRepo;

@Component
public class ApartmentProviderImpl implements ApartmentProvider {

	@Autowired
	private ApartmentRepo apartmentRepo;
	
	@Override
	public List<Apartment> getAllApartments() {
		return apartmentRepo.findAll();
	}

	@Override
	public Apartment getApartment(int aid) {
		Optional<Apartment> apartment = apartmentRepo.findById(aid);
		if(apartment!=null) {
			return apartment.get();
		} 
		
		return null;
	}

	@Override
	public boolean isExist(int aid) {
		if(apartmentRepo.existsById(aid)) {
			return true;
		}
		return false;
	}

}
