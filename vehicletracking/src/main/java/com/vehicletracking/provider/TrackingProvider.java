package com.vehicletracking.provider;

import java.util.Date;
import java.util.List;

import com.vehicletracking.model.Tracking;

public interface TrackingProvider {
	public Tracking save(Tracking tracking);
	public List<Tracking> getByApartment(int apartmentId);
	public List<Tracking> getAll();
	public List<Tracking> getAllByApartmentAndVehicle(int aid,String vid);
	public Tracking getAllByApartmentAndVehicleOutimeNull(int aid,String vid);
	public Tracking findByApartmentIdAndVehicleIdAndInAndOutTime(int apartmentId,String vehicleId); 
	public Tracking findByVehicleIdAndInTimeAndOutTimeNull(String vehicleId);
}
