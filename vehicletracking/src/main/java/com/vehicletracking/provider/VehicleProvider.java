package com.vehicletracking.provider;

import java.util.List;

import com.vehicletracking.model.Vehicle;

public interface VehicleProvider {
	public List<Vehicle> getAllVehicles();
	public Vehicle getVehicle(int aid); 
	public Vehicle saveVehicle(Vehicle vehicle);
	public Vehicle getByVechicleNumber(String vehicleNo);	
}
