package com.vehicletracking.provider;

import java.util.List;

import com.vehicletracking.model.Apartment;

public interface ApartmentProvider {
	public List<Apartment> getAllApartments();
	public Apartment getApartment(int aid);
	public boolean isExist(int aid);
}
