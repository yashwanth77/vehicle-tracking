package com.vehicletracking.provider;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vehicletracking.model.Tracking;
import com.vehicletracking.repository.TrackingRepo;

@Component 
public class TrackingProvideImpl implements TrackingProvider {
	
	@Autowired
	TrackingRepo trackingRepo;

	@Override
	public Tracking save(Tracking tracking) {
		// TODO Auto-generated method stub
		return trackingRepo.save(tracking);
	}

	@Override
	public List<Tracking> getAll() {
		// TODO Auto-generated method stub
		return trackingRepo.findAll();
	}

	@Override
	public List<Tracking> getAllByApartmentAndVehicle(int aid, String vid) {
		return null;
	}

	@Override
	public Tracking getAllByApartmentAndVehicleOutimeNull(int aid, String vid) {
		return trackingRepo.findByApartmentAndVehicle(aid, vid);
	}

	@Override
	public List<Tracking> getByApartment(int apartmentId) {
		return trackingRepo.findByApartment(apartmentId);
	}

	@Override
	public Tracking findByApartmentIdAndVehicleIdAndInAndOutTime(int apartmentId, String vehicleId) {
		return trackingRepo.findByApartmentIdAndVehicleIdAndInAndOutTime(apartmentId, vehicleId);
	}

	@Override
	public Tracking findByVehicleIdAndInTimeAndOutTimeNull(String vehicleId) {
		return trackingRepo.findByVehicleIdAndInTimeAndOutTimeNull(vehicleId);
	}

	
}
