package com.vehicletracking.provider;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vehicletracking.model.Vehicle;
import com.vehicletracking.repository.VehicleRepo;

@Component
public class VehicleProviderImpl implements VehicleProvider {

	@Autowired
	private VehicleRepo vehicleRepo;
	
	@Override
	public List<Vehicle> getAllVehicles() {
		return vehicleRepo.findAll();
	}

	@Override
	public Vehicle getVehicle(int vid) {
		// TODO Auto-generated method stub
		Optional<Vehicle> vehicle = vehicleRepo.findById(vid);
		if(vehicle!=null) {
			return vehicle.get();
		} 
		return null;	
	}

	@Override
	public Vehicle saveVehicle(Vehicle vehicle) {
		return vehicleRepo.save(vehicle);
	}

	@Override
	public Vehicle getByVechicleNumber(String vehicleNo) {
		// TODO Auto-generated method stub
		return vehicleRepo.findByVehicleNo(vehicleNo) ;
	}

	
}
