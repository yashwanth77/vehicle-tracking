package com.vehicletracking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicletracking.model.Apartment;
import com.vehicletracking.provider.ApartmentProvider;

@Service
public class ApartmentServiceImpl implements ApartmentService {
	
	@Autowired
	private ApartmentProvider apartmentProvider;
	
	@Override
	public List<Apartment> getAllApartments() {
		return apartmentProvider.getAllApartments();
	}

	@Override
	public Apartment getApartment(int apartmentId) {
		// TODO Auto-generated method stub
		return apartmentProvider.getApartment(apartmentId);
	}

}
