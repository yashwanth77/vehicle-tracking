package com.vehicletracking.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.vehicletracking.exception.BusinessException;
import com.vehicletracking.model.Apartment;
import com.vehicletracking.model.Tracking;
import com.vehicletracking.model.Vehicle;
import com.vehicletracking.provider.ApartmentProvider;
import com.vehicletracking.provider.TrackingProvider;
import com.vehicletracking.provider.VehicleProvider;
import com.vehicletracking.utility.AppError;
import com.vehicletracking.utility.ErrorConstant;
/**
 * 
 * @author yashwanth 
 * 
 */
@Service
public class TrackingServiceImpl implements TrackingService {

	@Autowired
	private TrackingProvider trackingProvider;
	
	@Autowired
	private ApartmentProvider apartmentProvider;
	
	@Autowired
	private VehicleProvider vehicleProvider;
	
	@Override
	public Tracking saveInVehicle(String vehicleNo, int apartmentId, Date inTime) {
		
		isApartmentAvailable(apartmentId);
		isValidEntry(vehicleNo);
		Tracking tracking = new Tracking();
		Apartment apartment = apartmentProvider.getApartment(apartmentId);
		Vehicle vehicle = vehicleProvider.getByVechicleNumber(vehicleNo);
		if(vehicle==null) {
			vehicle = new Vehicle();
			vehicle.setRegistered(false);
			vehicle.setVehicleNo(vehicleNo);
			vehicle = vehicleProvider.saveVehicle(vehicle);
		}
		
		tracking.setApartment(apartment);
		tracking.setVehicle(vehicle);
		tracking.setInTime(inTime);
		tracking.setOutTime(null);
	
		return trackingProvider.save(tracking);
	}

	@Override
	public Tracking saveOutVehicle(String vehicleNo, int apartmentId, Date outTime) {
		isApartmentAvailable(apartmentId);
		isVehicleInApartment(apartmentId,vehicleNo);
		Tracking tracking = trackingProvider.getAllByApartmentAndVehicleOutimeNull(apartmentId, vehicleNo);
		if(tracking==null) {
			return null;
		}
		tracking.setOutTime(outTime);
		return trackingProvider.save(tracking);
	}

	
	@Override
	public List<Tracking> getAllByApartmentAndVehicle(int aid, String vid, Date date) {
		return null;
	}

	@Override
	public List<Tracking> getByApartment(int apartmentId) {
		return trackingProvider.getByApartment(apartmentId);
	} 
	
	private boolean isApartmentAvailable(int apartmentId) {
		if(!apartmentProvider.isExist(apartmentId)) {
			throw new BusinessException(HttpStatus.BAD_REQUEST,new AppError(ErrorConstant.INVALID_APARTMENT.getCode(),ErrorConstant.INVALID_APARTMENT.getDescription()));
		}
		return true;
	} 
	private boolean isVehicleInApartment(int apartmentId,String vehicleId) {
		Tracking tracking = this.trackingProvider.findByApartmentIdAndVehicleIdAndInAndOutTime(apartmentId, vehicleId);
		if(tracking==null) {
			throw new BusinessException(HttpStatus.BAD_REQUEST,new AppError(ErrorConstant.VEHICLE_IN_DIFFERENT_APARTMENT.getCode(),ErrorConstant.VEHICLE_IN_DIFFERENT_APARTMENT.getDescription()));
		}
		return true;
	} 
	
	private boolean isValidEntry(String vehicleId) {
		Tracking tracking = this.trackingProvider.findByVehicleIdAndInTimeAndOutTimeNull(vehicleId);
		if(tracking!=null) {
			throw new BusinessException(HttpStatus.BAD_REQUEST,new AppError(ErrorConstant.VEHICLE_IN_DIFFERENT_APARTMENT.getCode(),ErrorConstant.VEHICLE_IN_DIFFERENT_APARTMENT.getDescription()));
		}
		return true;
	}

}
