package com.vehicletracking.service;

import java.util.List;

import com.vehicletracking.model.Vehicle;

public interface VehicleService {
	
	public List<Vehicle> getAllVehicles();
	public Vehicle getVehicle(int aid); 
}
