package com.vehicletracking.service;

import java.util.List;

import com.vehicletracking.model.Apartment;

public interface ApartmentService {
	public List<Apartment> getAllApartments();
	public Apartment getApartment(int apartmentId);
}
