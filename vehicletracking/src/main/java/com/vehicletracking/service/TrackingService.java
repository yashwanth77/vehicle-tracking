package com.vehicletracking.service;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.vehicletracking.model.Tracking;

public interface TrackingService {
	
	public Tracking saveInVehicle(String vehicleNo,int apartmentId,Date inTime);
	public Tracking saveOutVehicle(String vehicleNo,int apartmentId,Date outTime);
	public List<Tracking> getByApartment(int apartmentId);
	public List<Tracking> getAllByApartmentAndVehicle(int aid,String vid,Date date);
}
