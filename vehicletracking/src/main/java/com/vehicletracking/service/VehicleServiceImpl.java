package com.vehicletracking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicletracking.model.Vehicle;
import com.vehicletracking.provider.VehicleProvider;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleProvider vehicleProvider;

	@Override
	public List<Vehicle> getAllVehicles() {
		return vehicleProvider.getAllVehicles();
	}

	@Override
	public Vehicle getVehicle(int aid) {
		return vehicleProvider.getVehicle(aid);
	}

	

}
