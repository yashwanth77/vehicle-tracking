package com.vehicletracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vehicletracking.model.request.VehicleInAndOut;
import com.vehicletracking.service.ApartmentService;
import com.vehicletracking.service.TrackingService;
import com.vehicletracking.service.VehicleService;
import com.vehicletracking.utility.ApplicationResponse;

@RestController
public class AppController {
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private TrackingService trackingService;
		
	@GetMapping("/apartments")
	public ApplicationResponse getAllApartments() {
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(apartmentService.getAllApartments());
		Response.setError(null);
		Response.setSuccess(true);
		return Response;
	}  
	
	@GetMapping("/apartments/{aid}")
	public ApplicationResponse getAllApartments(@PathVariable("aid") int apartmentId) {
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(apartmentService.getApartment(apartmentId));
		Response.setError(null);
		Response.setSuccess(true);
		return Response;
	}  
	
	@GetMapping("/vehicles")
	public ApplicationResponse getAllVehicles() {
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(vehicleService.getAllVehicles());
		Response.setError(null);
		Response.setSuccess(true);
		return Response;
	}
	
	
	@PostMapping("/vehicles/intime")
	public ApplicationResponse saveVehicleInTime(@RequestBody VehicleInAndOut vehicleInAndOut) {
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(trackingService.saveInVehicle(vehicleInAndOut.getVehicleNo(), vehicleInAndOut.getApartmentId(), vehicleInAndOut.getDate()));
		Response.setError(null);
		Response.setSuccess(true);
		return Response;
	}
	
	@PostMapping("/vehicles/outtime")
	public ApplicationResponse saveVehicleOutTime(@RequestBody VehicleInAndOut vehicleInAndOut) {
		
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(trackingService.saveOutVehicle(vehicleInAndOut.getVehicleNo(),vehicleInAndOut.getApartmentId(),vehicleInAndOut.getDate()));
		Response.setError(null);
		Response.setSuccess(true);
		return Response;        
	}  
	
	@GetMapping("/apartments/{apartmentId}/trackings")
	public ApplicationResponse getAllVehicleForApartment(@PathVariable("apartmentId") int apartmentId ) {
		ApplicationResponse Response = new ApplicationResponse();
		Response.setData(trackingService.getByApartment(apartmentId));
		Response.setError(null);
		Response.setSuccess(true);
		return Response;
	}
	
	
}
