package com.vehicletracking.utility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Class used for representing response.
 */

@Builder
public class ApplicationResponse<T> {
  // Flag representing if the request is successful
  @JsonProperty("isSuccess")
  private boolean success;

  // Success data
  private T data;

  // Error data
  AppError error;
  
  public ApplicationResponse(){
	  
  }

public ApplicationResponse(boolean success, T data, AppError error) {
	super();
	this.success = success;
	this.data = data;
	this.error = error;
}

public boolean isSuccess() {
	return success;
}

public void setSuccess(boolean success) {
	this.success = success;
}

public T getData() {
	return data;
}

public void setData(T data) {
	this.data = data;
}

public AppError getError() {
	return error;
}

public void setError(AppError error) {
	this.error = error;
} 
  
  
  
}
