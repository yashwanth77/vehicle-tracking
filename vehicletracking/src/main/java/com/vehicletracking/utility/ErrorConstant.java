package com.vehicletracking.utility;

public enum ErrorConstant {
	
	INVALID_APARTMENT("INVALID APARTMENT ID","APARTMENT ID DOESNT EXIST"),
	VEHICLE_IN_DIFFERENT_APARTMENT("NOT VALID ENTRY","VEHICLE IN DIFFERENT APARTMENT"),
	INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR","SORRY FOR THE INCONVINIENCE");
	ErrorConstant(String string, String string2) {
	}
	String code;
	String description;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 
	

}
