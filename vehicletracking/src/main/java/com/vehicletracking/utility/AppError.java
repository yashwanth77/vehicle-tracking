package com.vehicletracking.utility;

import lombok.Getter;
import lombok.Setter;

/**
 * Class to represent an AppError.
 */
@Getter
@Setter
public class AppError {

  // Code describing the error
  private String code;

  // Message representing the error
  private String description;
public AppError() {
	
}
public AppError(String code, String description) {
	super();
	this.code = code;
	this.description = description;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
} 
  
  
}
