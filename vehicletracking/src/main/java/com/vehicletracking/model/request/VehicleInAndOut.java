package com.vehicletracking.model.request;

import java.sql.Time;
import java.util.Date;

public class VehicleInAndOut {

	private String vehicleNo;
	private int apartmentId;
	private Date date;
	public VehicleInAndOut(){
		
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public int getApartmentId() {
		return apartmentId;
	}
	public void setApartmentId(int apartmentId) {
		this.apartmentId = apartmentId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}	
	
	
}
