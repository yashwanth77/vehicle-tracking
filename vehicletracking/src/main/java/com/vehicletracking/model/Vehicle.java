package com.vehicletracking.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Vehicle")
public class Vehicle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicleId")
	private Integer vehicleId;
	
	@Column(name = "vehicleType")
	private String vehicleType; 
	
	@Column(name = "vehicleNo")
	private String vehicleNo;
	
	@Column(name = "registered")
	private Boolean registered;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdOn")
	@CreationTimestamp
	private Date createdOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "updatedOn")
	private Date updatedOn;

	
	
	public Vehicle() {
		
	}



	public Integer getVehicleId() {
		return vehicleId;
	}



	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}



	public String getVehicleType() {
		return vehicleType;
	}



	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}



	public String getVehicleNo() {
		return vehicleNo;
	}



	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}



	public Boolean getRegistered() {
		return registered;
	}



	public void setRegistered(Boolean registered) {
		this.registered = registered;
	}



	public Date getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public Date getUpdatedOn() {
		return updatedOn;
	}



	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

			
	
}
