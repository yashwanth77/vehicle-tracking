package com.vehicletracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vehicletracking.model.Apartment;

@Repository
public interface ApartmentRepo extends JpaRepository<Apartment, Integer> {

}
