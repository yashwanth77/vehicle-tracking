package com.vehicletracking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vehicletracking.model.Tracking;

@Repository
public interface TrackingRepo extends JpaRepository<Tracking, Integer> {
	
	@Query(nativeQuery=true,value="SELECT * FROM tracking where apartmentid=:apartmentId and vehicleid = (select vehicle_id from vehicle where vehicle.vehicle_no =:vehicleId) and out_time is NULL")
	public Tracking findByApartmentAndVehicle(int apartmentId,String vehicleId);
	
	@Query(nativeQuery=true,value="SELECT * FROM tracking where apartmentid=:apartmentId")
	public List<Tracking> findByApartment(int apartmentId); 
	
	@Query(nativeQuery=true,value="SELECT * FROM tracking WHERE tracking.in_time IS NOT NULL AND tracking.out_time IS NULL  AND  tracking.apartmentid=:apartmentId AND tracking.vehicleid = (SELECT vehicle.vehicle_id FROM vehicle WHERE vehicle.vehicle_no=:vehicleNo);\n" + 
			"")
	public Tracking findByApartmentIdAndVehicleIdAndInAndOutTime(int apartmentId,String vehicleNo); 
	
	@Query(nativeQuery=true,value="SELECT * FROM tracking WHERE tracking.in_time IS NOT NULL AND tracking.out_time IS NULL  AND tracking.vehicleid= (SELECT vehicle.vehicle_id FROM vehicle WHERE vehicle.vehicle_no=:vehicleNo);")
	public Tracking findByVehicleIdAndInTimeAndOutTimeNull(String vehicleNo); 
	
}
