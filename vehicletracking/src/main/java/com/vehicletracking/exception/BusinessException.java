package com.vehicletracking.exception;

import org.springframework.http.HttpStatus;

import com.vehicletracking.utility.AppError;

public class BusinessException extends RuntimeException {
	
	 /**
	   * Code to represent the exception.
	   */
	  private final String code;

	  /**
	   * AppError that is thrown.
	   */
	  private final AppError appError;

	  /**
	   * HTTP Status code to set for the response.
	   */
	  private final HttpStatus httpStatus;
	  
	  
	  /**
	   * Constructs a new exception with the specified Business Error.
	   *
	   * @param error Should be a valid @{@link BusinessErrors}
	   *
	   *              <p>All the errors should have a proper BusinessErrors entry
	   *              and BusinessException cannot be
	   *              invoked with random errorCode and errorMessage.</p>
	   */
	  public BusinessException(HttpStatus httpStatus, AppError error) {
	    this(httpStatus, error, null);
	  }

	  /**
	   * Parameterized Constructor.
	   *
	   * @param httpStatus - httpStatus.
	   * @param error      - error.
	   * @param cause      - cause.
	   */
	  public BusinessException(HttpStatus httpStatus, AppError error, Throwable cause) {
	    super(error.getDescription(), cause);
	    this.httpStatus = httpStatus;
	    this.code = error.getCode();
	    this.appError = error;
	  }

	public String getCode() {
		return code;
	}

	public AppError getAppError() {
		return appError;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	} 
	  
	  



}
