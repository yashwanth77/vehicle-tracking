package com.vehicletracking.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.vehicletracking.exception.BusinessException;
import com.vehicletracking.utility.AppError;
import com.vehicletracking.utility.ApplicationResponse;
import com.vehicletracking.utility.ErrorConstant;

@ControllerAdvice
@SuppressWarnings({"unchecked","rawtypes"})
public class CustomExceptionhandler<T>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        AppError appError = new AppError(ErrorConstant.INTERNAL_SERVER_ERROR.getCode(),ErrorConstant.INTERNAL_SERVER_ERROR.getDescription());  
        return new ResponseEntity<>(ErrorConstant.INTERNAL_SERVER_ERROR.getDescription(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
 
    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(BusinessException ex, WebRequest request) {
        System.out.println("business exeception");
    	List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        return new ResponseEntity<>(ex.getAppError().getDescription(),HttpStatus.BAD_REQUEST);
    }
}
